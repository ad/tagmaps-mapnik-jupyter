"""Tag Maps Jupyter Notebook Tools"""

import csv
import warnings
import pandas as pd
import geoviews as gv
import geopandas as gp
import numpy as np
import mapclassify as mc
from cartopy import crs
from typing import List, Tuple, Dict, Optional
from pathlib import Path
from collections import namedtuple
from IPython.display import display
from matplotlib import colors

def _rnd_f(f: float, dec: int = None) -> str:
    if dec is None:
        # return f'{f}'
        dec = 0
    return f'{f:,.{dec}f}'

def format_bound(
    upper_bound: float = None, lower_bound: float = None, 
    decimals: Optional[int] = None) -> str:
    """Format legend text for class bounds"""
    if upper_bound is None:
        return _rnd_f(lower_bound, decimals)
    if lower_bound is None:
        return _rnd_f(upper_bound, decimals)
    return f'{_rnd_f(lower_bound, decimals)} - {_rnd_f(upper_bound, decimals)}'

def min_decimals(num1: float, num2: float) -> int:
    """Return number of minimum required decimals"""
    if _rnd_f(num1) != _rnd_f(num2):
        return 0
    for i in range(1, 5):
        if _rnd_f(num1, i) != _rnd_f(num2, i):
            return i
    return 5

def get_label_bounds(
    scheme_classes: np.ndarray, metric_series: pd.Series,
    flat: bool = None) -> List[str]:
    """Get all upper bounds in the scheme_classes category"""
    upper_bounds = scheme_classes
    # get and format all bounds
    bounds = []
    for idx, upper_bound in enumerate(upper_bounds):
        if idx == 0:
            lower_bound = metric_series.min()
            decimals = None
        else:
            decimals = min_decimals(
                upper_bounds[idx-1], upper_bounds[idx])
            lower_bound = upper_bounds[idx-1]
        if flat:
            bound = format_bound(
                lower_bound=lower_bound,
                decimals=decimals)
        else:
            bound = format_bound(
                upper_bound, lower_bound,
                decimals=decimals)
        bounds.append(bound)
    if flat:
        upper_bound = format_bound(
            upper_bound=upper_bounds[-1],
            decimals=decimals)
        bounds.append(upper_bound)
    return bounds

def classify_data(values: np.ndarray, scheme: str):
    """Classify data (value series) and return classes,
       bounds, and colormap
       
    Args:
        grid: A geopandas geodataframe with metric column to classify
        metric: The metric column to classify values
        scheme: The classification scheme to use.
        mask_nonsignificant: If True, removes non-significant values
            before classifying
        mask_negative: Only consider positive values.
        mask_positive: Only consider negative values.
        cmap_name: The colormap to use.
        return_cmap: if False, returns list instead of mpl.ListedColormap
        store_classes: Update classes in original grid (_cat column). If
            not set, no modifications will be made to grid.
        
    Adapted from:
        https://stackoverflow.com/a/58160985/4556479
    See available colormaps:
        http://holoviews.org/user_guide/Colormaps.html
    See available classification schemes:
        https://pysal.org/mapclassify/api.html
        
    Notes: some classification schemes (e.g. HeadTailBreaks)
        do not support specifying the number of classes returned
        construct optional kwargs with k == number of classes
    """
    optional_kwargs = {"k":7}
    if scheme == "HeadTailBreaks":
        optional_kwargs = {}
    scheme_breaks = mc.classify(
        y=values, scheme=scheme, **optional_kwargs)
    return scheme_breaks
    
def get_hex_col(cmap) -> List[str]:
    """Return list of hex colors for cmap"""
    return [colors.rgb2hex(cmap(i)) for i in range(cmap.N)]

def get_cmap_list(
        cmap_name: str, length_n: int) -> [str]:
    """Create a classified colormap of length N
    """
    if cmap_name in ["OrRd", "Blues"]:
        # special color-mod for OrRd and Blues
        cmap = plt.cm.get_cmap(cmap_name, length_n)
        cmap_list = get_hex_col(cmap)
        cmap_color_contrast(cmap_list, cmap_name)
        return cmap_list
    # general case:
    # increase contrast of first color,
    # by extending and pop(0)
    cmap = plt.cm.get_cmap(cmap_name, length_n+1)
    cmap_list = get_hex_col(cmap)
    cmap_list.pop(0)
    if length_n != len (cmap_list):
        warnings.warn("Cmap list length mismatch")
    return cmap_list

FileStat = namedtuple('File_stat', 'name, size, records')

def get_file_stats(name: str, file: Path) -> Tuple[str, str, str]:
    """Get number of records and size of CSV file"""
    num_lines = f'{sum(1 for line in open(file)):,}'
    size = file.stat().st_size
    size_gb = size/(1024*1024*1024)
    size_format = f'{size_gb:.2f} GB'
    size_mb = None
    if size_gb < 1:
        size_mb = size/(1024*1024)
        size_format = f'{size_mb:.2f} MB'
    if size_mb and size_mb < 1:
        size_kb = size/(1024)
        size_format = f'{size_kb:.2f} KB'
    return FileStat(name, size_format, num_lines)

def display_file_stats(filelist: Dict[str, Path]):
    """Display CSV """
    df = pd.DataFrame(
        data=[
            get_file_stats(name, file) for name, file in filelist.items()
            ]).transpose()
    header = df.iloc[0]
    df = df[1:]
    df.columns = header
    display(df.style.background_gradient(cmap='viridis'))

def apply_formatting(col, hex_colors):
    """Apply background-colors to pandas columns"""
    for hex_color in hex_colors:
        if col.name == hex_color:
            return [f'background-color: {hex_color}' for c in col.values]

def apply_formatting_num(col, hex_colors, as_id_list):
    """Apply background-colors to pandas columns (id variant)"""
    for ix, id in enumerate(as_id_list):
        if col.name == id:
            return [f'background-color: {hex_colors[ix]}' for c in col.values]
        
def display_hex_colors(hex_colors: List[str], as_id: bool = None):
    """Visualize a list of hex colors using pandas. Use
    as_id=True to output a table with equal-width cols, useful for legends"""
    df = pd.DataFrame(hex_colors).T
    if as_id:
        as_id_list = [f'{x:05d}' for x in list(range(0, len(hex_colors)))]
        df.columns = as_id_list
    else:
        df.columns = hex_colors
    df.iloc[0,0:len(hex_colors)] = ""
    df.rename(index={0: 'Colors'}, inplace=True)
    if as_id:
        display(df.style.apply(lambda x: apply_formatting_num(x, hex_colors, as_id_list)))
        return
    display(df.style.apply(lambda x: apply_formatting(x, hex_colors)))


