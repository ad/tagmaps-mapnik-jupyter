[![version](https://ad.vgiscience.org/tagmaps-mapnik-jupyter/version.svg)][static-gl-url] [![pipeline](https://ad.vgiscience.org/tagmaps-mapnik-jupyter/pipeline.svg)][static-gl-url]

# Tag Maps rending with mapnik in a Jupyter Notebook

![final map](https://ad.vgiscience.org/tagmaps-mapnik-jupyter/mapnik_tagmap.png)

Repsitory containing jupyter notebook guide to tagmaps rendering with [Mapnik][mapnik]. The notebook is stored as a markdown file, with [jupytext][1], for better git compatibility.
The notebook can be run with [jupyterlab-docker][2].

____

- HTML version of Notebook: **[01_mapnik-tagmaps.html][nb_01]**

## Convert to ipynb files

First, either download release files or convert the markdown files to working jupyter notebooks.

If you're using the [docker image][2], the notebooks will be generated automatically in the `notebooks/` folder.

To convert jupytext markdown files to ipynb-format manually, open a terminal and follow these commands:

```bash
mkdir notebooks
jupytext --set-formats notebooks///ipynb,md///md,py///_/.py --sync md/01_mapnik-tagmaps.md
```

[1]: https://github.com/mwouts/jupytext
[2]: https://gitlab.vgiscience.de/lbsn/tools/jupyterlab
[nb_01]: https://ad.vgiscience.org/tagmaps-mapnik-jupyter/01_mapnik-tagmaps.html
[static-gl-url]: https://gitlab.vgiscience.de/ad/tagmaps-mapnik-jupyter
[mapnik]: https://github.com/mapnik