#!/usr/bin/env python3

import mapnik

mapnik.register_fonts('/fonts')
# e.g.
# mapnik.register_fonts('/c/Windows/Fonts/')
# mapnik.register_fonts('~/.fonts/')
# for face in mapnik.FontEngine.face_names(): print(face)

stylesheet = 'tagmap_style.xml'
image = 'intermediate/tagmap_style.png'
m = mapnik.Map(2500,1400)
mapnik.load_map(m, stylesheet)
m.zoom_all() 
mapnik.render_to_file(m, image)

